# Mr. Wendigo's Journey

Mr. Wendigo's Journey (MWJ) is a public domain 2D platformer game heavily inspired by the likes of SuperTux and Super Mario Bros, made for want of a good, public domain substitute.

Much like my other projects, MWJ has the following:
- Separate game code from the frontend, for easy porting
- Drawing is handled by the frontend, making the game code completely function-only
- Coded entirely in C, uses TCC for Windows and GCC for Linux
- So far, only tested with SDL2

## Making Levels

MWJ has its own level editor made with ncurses that you can use to make externally saved level data. A script can be used to convert such level data to C code so you may compile the game sucklessly.

## TO-DO:
- Add a macro for the highest integer type that you can use, like GAME\_INT\_TYPE or something. Good for certain systems.
- Port to SAF, mind the resolution
- Make a level editor (done-ish, needs more polish)
- Optionally, add a way to pack all or most of the essential assets into the program via code (aka suckless mode)

## License

I, blitzdoughnuts, license this project and its original contents under CC0, and waive all rights to the public for whatever use. Public domain, if you will.

The repo currently includes the song "Long Way Home" found on OpenGameArt, licensed under CC0.
