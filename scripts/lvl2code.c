/*
MWJ - Level File to Code

Turn a level into C code for the Suckless mode

Reads a level from STDIN and outputs code to STDOUT
In BASH, you can do lvl.dat | lvl2code > code.h
*/
