Level Editor Controls:

GBVN moves whatever object you have selected like WASD or arrow keys

E toggles the presence of a certain index of object/solid

F switches your edit mode: 0 edits objects, 1 edits solids
