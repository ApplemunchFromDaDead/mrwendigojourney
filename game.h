#define KEY_JUMP 1
#define KEY_RUN 1 << 1
#define KEY_UP 1 << 2
#define KEY_DN 1 << 3
#define KEY_LF 1 << 4
#define KEY_RT 1 << 5
#define KEY_MENU 1 << 6

#define PFLAG_GROUNDED 1
#define PFLAG_CANMOVE 2
#define PFLAG_FLIPPED 4

#define INTERTYPE_BRICK 0
#define INTERTYPE_ITEMBLOCK 1
#define INTERTYPE_ITEMLOOSE 2
#define INTERTYPE_COIN 3
// ENEMIES
#define INTERTYPE_WALKYGUY 64
#define INTERTYPE_SHELLGUY 65

#define INTER_ACTIVE 1
#define INTER_GROUNDED 1 << 7

#define GAME_NAME "Mr. Wendigo's Journey"
#define GAME_WIDTH 240
#define GAME_HEIGHT 135

#define MWJ_SUCKLESS_MODE 0
// disables any form of dynamic configuration; obsoletes game options like CONF
#define MWJ_VERSION 0

#define GAME_OPTS_LOW 1 // disables all sorts of cosmetics and unnecessary junk
#define GAME_OPTS_CONF 1 << 1 // allow use of custom-format .txt configs (DEPENDS ON FRONTEND)

#define withinRange(x1, y1, x2, y2, dist) (x2 - x1 > dist && x2 - x1 < -dist) && (y2 - y1 > dist && y2 - y1 < -dist)

int8_t addInteractible(uint8_t newtype);

uint8_t input_keys;
uint8_t coins;
uint16_t score;

uint8_t LVL_FLAGS;

typedef struct {
	int x1, y1, x2, y2;
} COLBOX;

typedef struct {
	int x, y;
	int8_t hsp, vsp; // these go up to 32 in 1/8ths, basically a fixed point value
	uint8_t pflags, animframe, animtimer, jumpcount;
	COLBOX colbox;
} GPlayer;
GPlayer plr;

int cam_x, cam_y;

/*
to-do: GEnemy struct with several types
*/

typedef struct {
	int x, y;
	int8_t hsp, vsp;
	uint8_t flags, vars[8], type;
	COLBOX colbox;
} GInteractible;
GInteractible interacts[64];
uint8_t interacts_count;

COLBOX lvlcollision[256];
uint8_t lvlcol_flags[256];
uint8_t lvlcollision_count; // dictated by the loaded level

#include "level.h"

void debug_printColBoxPoints(COLBOX *in, const char *tag) {
	printf(tag);
	printf(": %i, %i, %i, %i\n", in->x1, in->x2, in->y1, in->y2);
};

void playSFX(uint8_t index);

// --- COLLISION ---

static uint8_t checkBoxCollision(COLBOX *box1, COLBOX *box2, int16_t box1_xoffset, int16_t box1_yoffset) {
    return (!(
    	box2->x1 >= box1->x2 + box1_xoffset ||
    	box2->x2 <= box1->x1 + box1_xoffset ||
    	box2->y1 >= box1->y2 + box1_yoffset ||
    	box2->y2 <= box1->y1 + box1_yoffset
    ));
};

static uint8_t checkPointCollision(int px, int py, COLBOX *box) {
    return ( ( px >= box->x1 && px <= box->x2)
    && ( py >= box->y1 && py <= box->y2 ) );
};

// --- INTERACTIBLE CODES ---

void inter_checkCollide(GInteractible *REF);

void interact_step(GInteractible *REF) {
	switch (REF->type)
	{
		case 0: default:
			
			break;
		case INTERTYPE_WALKYGUY: case INTERTYPE_SHELLGUY:
			inter_checkCollide(REF);
			break;
	}
};

int8_t addInteractible(uint8_t newtype) {
	for (uint8_t i = 0; i < interacts_count + 1; i++) {
		if (interacts[i].flags & INTER_ACTIVE) continue;
		interacts[i].flags ^= INTER_ACTIVE;
		interacts[i].type = newtype;
		interacts[i].vars[0] = 0;
		interacts[i].vars[1] = 0;
		interacts[i].vars[2] = 0;
		interacts[i].vars[3] = 0;
		interacts[i].vars[4] = 0;
		interacts[i].vars[5] = 0;
		interacts[i].vars[6] = 0;
		interacts[i].vars[7] = 0;
		interacts[i].colbox.x1 = interacts[i].x;
		interacts[i].colbox.y1 = interacts[i].y;
		interacts[i].colbox.x2 = interacts[i].x + 16;
		interacts[i].colbox.y2 = interacts[i].y + 16;
		interacts_count++;
		return i;
	};
	return -1;
};

int addCollisionBox(int x, int y, int w, int h) {
	for (uint8_t i = 0; i < lvlcollision_count + 1; i++) {
		if (lvlcol_flags[i] & INTER_ACTIVE) continue;
		lvlcollision[i].x1 = x;
		lvlcollision[i].y1 = y;
		lvlcollision[i].x2 = x + (16 * w);
		lvlcollision[i].y2 = y + (16 * h);
		lvlcol_flags[i] ^= INTER_ACTIVE;
		printf("Added collide: index %i\n", i);
		lvlcollision_count++;
		return i;
	};
	return -1;
};

void plr_checkCollide() {
	debug_printColBoxPoints(&plr.colbox, "Player collision");
	
	if (plr.pflags & PFLAG_GROUNDED) plr.pflags ^= PFLAG_GROUNDED;
	for (uint8_t i = 0; i < 255; i++) {
		if (!(lvlcol_flags[i] & INTER_ACTIVE)) continue;
		if (checkBoxCollision(&plr.colbox, &lvlcollision[i], plr.hsp >> 3, 0)) {
			int8_t plr_dir = (plr.hsp <= 0) ? -1 : 1;
			while (!checkBoxCollision(&plr.colbox, &lvlcollision[i], plr_dir, 0)) {
				plr.x += plr_dir;
				plr.colbox.x1 = plr.x;
				plr.colbox.x2 = plr.x + 16;
			};
			plr.hsp = 0;
		}
		
		if (checkBoxCollision(&plr.colbox, &lvlcollision[i], 0, plr.vsp >> 3)) {
			int8_t plr_dir = (plr.vsp <= 0) ? -1 : 1;
			while (!checkBoxCollision(&plr.colbox, &lvlcollision[i], 0, plr_dir)) {
				plr.y += plr_dir;
				plr.colbox.y1 = plr.y;
				plr.colbox.y2 = plr.y + 16;
			};
			plr.vsp = 0;
		}
		if (checkPointCollision(plr.x + 8, plr.y + 15, &lvlcollision[i])) {
			if (!(plr.pflags & PFLAG_GROUNDED)) plr.pflags ^= PFLAG_GROUNDED;
		};
	};
	
	for (uint8_t i = 0; i < 63; i++) {
		if (!(interacts[i].type & INTERTYPE_BRICK | INTERTYPE_ITEMBLOCK) || !(interacts[i].flags & INTER_ACTIVE)) continue;
		if (checkBoxCollision(&plr.colbox, &interacts[i].colbox, plr.hsp >> 3, 0)) {
			int8_t plr_dir = (plr.hsp < 0) ? -1 : 1;
			while (!checkBoxCollision(&plr.colbox, &interacts[i].colbox, plr_dir, 0)) {
				plr.x += plr_dir;
				plr.colbox.x1 = plr.x;
				plr.colbox.x2 = plr.x + 16;
			};
			plr.hsp = 0;
		}
		
		if (checkBoxCollision(&plr.colbox, &interacts[i].colbox, 0, plr.vsp >> 3)) {
			int8_t plr_dir = (plr.vsp < 0) ? -1 : 1;
			while (!checkBoxCollision(&plr.colbox, &interacts[i].colbox, 0, plr_dir)) {
				plr.y += plr_dir;
				plr.colbox.y1 = plr.y;
				plr.colbox.y2 = plr.y + 16;
			};
			plr.vsp = 0;
		}
		if (checkPointCollision(plr.x + 8, plr.y + 15, &interacts[i].colbox)) {
			if (!(plr.pflags & PFLAG_GROUNDED)) plr.pflags ^= PFLAG_GROUNDED;
		};
	};
	
	plr.x += plr.hsp / 8;
	plr.y += plr.vsp / 8;
	plr.colbox.x1 = plr.x + 2;
	plr.colbox.x2 = plr.x + 14;
	plr.colbox.y1 = plr.y;
	plr.colbox.y2 = plr.y + 15;
};

void inter_checkCollide(GInteractible *REF) { // called only by specific Interactibes' steps
	debug_printColBoxPoints(&REF->colbox, "Interactible collision");
	for (uint8_t i = 0; i < 255; i++) {
		if (!(lvlcol_flags[i] & INTER_ACTIVE)) continue;
		if (checkBoxCollision(&REF->colbox, &lvlcollision[i], REF->hsp >> 3, 0)) {
			int8_t plr_dir = (REF->hsp < 0) ? -1 : 1;
			while (!checkBoxCollision(&REF->colbox, &lvlcollision[i], plr_dir, 0)) {
				REF->x += plr_dir;
				REF->colbox.x1 = REF->x;
				REF->colbox.x2 = REF->x + 16;
			};
			REF->hsp = 0;
		}
		
		if (checkBoxCollision(&REF->colbox, &lvlcollision[i], 0, REF->vsp >> 3)) {
			int8_t plr_dir = (REF->vsp < 0) ? -1 : 1;
			while (!checkBoxCollision(&REF->colbox, &lvlcollision[i], 0, plr_dir)) {
				REF->y += plr_dir;
				REF->colbox.y1 = REF->y;
				REF->colbox.y2 = REF->y + 16;
			};
			REF->vsp = 0;
		}
		if (checkPointCollision(REF->x + 8, REF->y + 17, &lvlcollision[i])) {
			if (!(REF->flags & INTER_GROUNDED)) REF->flags ^= INTER_GROUNDED;
		} else {
			if (REF->flags & INTER_GROUNDED) REF->flags ^= INTER_GROUNDED;
		};
	};
	
	REF->x += REF->hsp >> 3;
	REF->y += REF->vsp >> 3;
	REF->colbox.x1 = REF->x;
	REF->colbox.x2 = REF->x + 16;
	REF->colbox.y1 = REF->y;
	REF->colbox.y2 = REF->y + 15;
};

void start() {
	/*
	addInteractible(INTERTYPE_BRICK);
	interacts[0].x = 128;
	interacts[0].y = 64;
	interacts[0].colbox.x1 = interacts[0].x;
	interacts[0].colbox.y1 = interacts[0].y;
	interacts[0].colbox.x2 = interacts[0].x + 16;
	interacts[0].colbox.y2 = interacts[0].y + 16;
	addInteractible(INTERTYPE_WALKYGUY);
	interacts[1].x = 150;
	interacts[1].y = 96;
	interacts[1].colbox.x1 = interacts[1].x;
	interacts[1].colbox.y1 = interacts[1].y;
	interacts[1].colbox.x2 = interacts[1].x + 16;
	interacts[1].colbox.y2 = interacts[1].y + 16;
	addInteractible(INTERTYPE_SHELLGUY);
	interacts[2].x = 180;
	interacts[2].y = 96;
	interacts[2].colbox.x1 = interacts[2].x;
	interacts[2].colbox.y1 = interacts[2].y;
	interacts[2].colbox.x2 = interacts[2].x + 16;
	interacts[2].colbox.y2 = interacts[2].y + 16;
	*/
	plr.colbox.x1 = plr.x;
	plr.colbox.y1 = plr.y;
	plr.colbox.x2 = plr.x + 16;
	plr.colbox.y2 = plr.y + 16;
	//addCollisionBox(0, 111, 100, 2);
	loadLevel("lvl.dat", 16);
	plr.x = 64;
	plr.y = 320;
};

void step() {
	/*
	NOTE: When handling fixed-point speed,
	think with eigths!
	
	1 subunit = 1/8 int
	1 pixel = 8 subunits (or 1)
	*/
	
	for (uint8_t i = 0; i < 63; i++) {
		if (!(interacts[i].flags & INTER_ACTIVE)) continue;
		switch (interacts[i].type) {
			case INTERTYPE_BRICK:
			case INTERTYPE_ITEMBLOCK:
				if (interacts[i].vars[1]) continue; // block was hit? skip it
				if (plr.pflags & PFLAG_GROUNDED) continue; // player's on the ground? why the hell would they be hitting a block?
				if (interacts[i].type == INTERTYPE_ITEMBLOCK && (interacts[i].flags & 2)) continue;
				if (checkBoxCollision(&plr.colbox, &interacts[i].colbox, 0, -1) && plr.y > interacts[i].y) {
						if (interacts[i].type == INTERTYPE_BRICK)interacts[i].flags ^= INTER_ACTIVE;
						else interacts[i].vars[1] = 1;
						score += 5;
						plr.vsp = 0;
						playSFX(3);
						puts("Brick hit");
					};
				break;
			case INTERTYPE_WALKYGUY: case INTERTYPE_SHELLGUY:
				if (checkBoxCollision(&plr.colbox, &interacts[i].colbox, 0, 0)) {
					if (interacts[i].y - plr.y > 0) {
						if (plr.pflags & PFLAG_GROUNDED || plr.vsp < 0) continue;
						score += 10;
						interacts[i].flags ^= INTER_ACTIVE;
						printf("Hit enemy! Player Y: %i Enemy Y: %i\n", plr.y, interacts[i].y);
						plr.vsp = -plr.vsp;
						plr.jumpcount = 12;
						playSFX(2);
					};
				};
				break;
		}
	};
	/*if (plr.y > 110) {
		plr.y = 110;
		plr.vsp = 0;
		plr.pflags ^= PFLAG_GROUNDED;
	};*/
	
	
	if (!(input_keys & KEY_LF) && !(input_keys & KEY_RT)) {
		if (plr.hsp != 0) plr.hsp = 0;
		plr.animframe = plr.animtimer = 0;
	};
	uint8_t speedlimit = (input_keys & KEY_RUN) ? 8 : 6;
	if (input_keys & KEY_LF) {
		plr.hsp -= (input_keys & KEY_RUN) ? 2 : 1;
		if (!(plr.pflags & PFLAG_FLIPPED)) plr.pflags ^= PFLAG_FLIPPED;
		plr.animtimer -= plr.hsp >> 3;
	};
	if (input_keys & KEY_RT) {
		plr.hsp += (input_keys & KEY_RUN) ? 2 : 1;
		if (plr.pflags & PFLAG_FLIPPED) plr.pflags ^= PFLAG_FLIPPED;
		plr.animtimer += plr.hsp >> 3;
	};
		speedlimit = 6;
	if (plr.animtimer >= 12) {
		plr.animframe++;
		plr.animtimer = 0;
	};
	if (plr.animframe > 2) plr.animframe = 0;
	if (!(plr.pflags & PFLAG_GROUNDED)) {
		plr.animframe = 4;
		plr.vsp += 4;
	};
	if (input_keys & KEY_JUMP) {
		if (plr.pflags & PFLAG_GROUNDED) {
			plr.y--; // stops him from jumping several times at once
			plr.jumpcount = 12;
			plr.pflags ^= PFLAG_GROUNDED;
			playSFX(0);
		};
		if (plr.jumpcount) {
			plr.vsp = -4 << 3;
			plr.jumpcount--;
		};
	};
	if (!(input_keys & KEY_JUMP) && plr.jumpcount) plr.jumpcount = 0;
	
	plr_checkCollide();
	if (plr.hsp < -speedlimit * 8 || plr.hsp > speedlimit * 8) plr.hsp = (plr.hsp < 0) ? -(speedlimit * 8) : speedlimit * 8;
	if (plr.vsp < -6 * 8 || plr.vsp > 6 * 8) plr.vsp = (plr.vsp <= -1) ? -6 * 8 : 6 * 8;
	
	if (plr.x > cam_x + (GAME_WIDTH - 64)) {
		cam_x += plr.x - (cam_x + (GAME_WIDTH - 64));
	};
	if (plr.x < cam_x + 64) {
		cam_x -= (cam_x + 64) - plr.x;
	};
	if (plr.y > cam_y + (GAME_HEIGHT - 64)) {
		cam_y += plr.y - (cam_y + (GAME_HEIGHT - 64));
	};
	if (plr.y < cam_y + 64) {
		cam_y -= (cam_y + 64) - plr.y;
	};
	if (cam_x < 0) cam_x = 0;
	if (cam_y > 320) cam_y = 320;
	
	for (uint8_t i; i < interacts_count; i++) {
		interact_step(&interacts[i]);
	};
	
	input_keys = 0;
};
