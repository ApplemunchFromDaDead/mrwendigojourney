/* MAIN_SDL
SDL frontend for the game. Requires SDL2, obviously.
*/

#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_image.h>
#undef main // TinyC doesn't like SDL's main
#include "game.h"

SDL_Rect dspdest_rect; // this is the DESTINATION, aka screen position
SDL_Rect fade_rect; // used for fading in/out
SDL_Rect rect_draw;

SDL_Point zeropoint = {0,0};

SDL_Window *win;
SDL_Renderer *render;

SDL_Texture *sprites[4];

Mix_Chunk *sfx[4];
Mix_Music *music[2];

uint8_t *keystates;

void playSFX(uint8_t index) {
	Mix_PlayChannel(-1, sfx[index], 0);
};

void drawSprite(SDL_Texture *sprite, int x, int y) {
	dspdest_rect = (SDL_Rect){x, y, 0, 0};
	SDL_QueryTexture(sprite, NULL, NULL, &dspdest_rect.w, &dspdest_rect.h);
	SDL_RenderCopy(render, sprite, NULL, &dspdest_rect);
};

void drawRepeatingSprite(SDL_Texture *sprite, int x, int y) {
	int _x = x;
	if (x == y) {
		dspdest_rect.x = dspdest_rect.y = x;
	} else {
		dspdest_rect.x = x;
		dspdest_rect.y = y;
	}
	SDL_QueryTexture(sprite, NULL, NULL, &dspdest_rect.w, &dspdest_rect.h);
	uint8_t limit = 0;
	for (; _x < GAME_WIDTH && limit <= 30; limit++) {
		if (_x < (-GAME_WIDTH << 2)) {
			_x += dspdest_rect.w;
			continue;
		};
		SDL_RenderCopy(render, sprite, NULL, &dspdest_rect);
		_x += dspdest_rect.w;
		dspdest_rect.x = _x;
	};
	limit = 0;
	_x = x;
	for (; _x > -dspdest_rect.w && limit <= 30; limit++) {
		SDL_RenderCopy(render, sprite, NULL, &dspdest_rect);
		_x -= dspdest_rect.w;
		dspdest_rect.x = _x;
	};
};

void drawSpriteSheeted(SDL_Texture *sprite, int x, int y, int frame, int x_sprdist, int y_sprdist, uint8_t flipped) { // supports only purely horizontal sheets
	if (x < -x_sprdist || x > x_sprdist + GAME_WIDTH) return;
	SDL_Rect destrect = {x, y, x_sprdist, y_sprdist};
	dspdest_rect.x = x_sprdist * frame;
	dspdest_rect.y = 0;
	dspdest_rect.w = x_sprdist,
	dspdest_rect.h = y_sprdist,
	SDL_RenderCopyEx(render, sprite, &dspdest_rect, &destrect, 0, &zeropoint, flipped ? SDL_FLIP_HORIZONTAL : SDL_FLIP_NONE);
};

void drawColBox(COLBOX boxin, uint8_t useCam) {
    rect_draw.x = boxin.x1;
    rect_draw.y = boxin.y1;
    rect_draw.w = boxin.x2 - boxin.x1;
    rect_draw.h = boxin.y2 - boxin.y1;
    if (useCam) {
    	rect_draw.x -= cam_x;
    	rect_draw.y -= cam_y;
    };
    SDL_RenderDrawRect(render, &rect_draw);
};

void drawTextString(const char *stuff, int x, int y) {
	// to-do: not this shit.
	// I should get to optimizing this
	SDL_SetTextureColorMod(sprites[0], 255, 255, 255);
	uint16_t _x = x;
    for (uint8_t i = 0; i < 255; i++) {
        if (stuff[i] == '\0') break; // terminator character? then get outta there before stuff gets nasty
        drawSpriteSheeted(sprites[0], _x, y, stuff[i]- 32, 10, 10, SDL_FLIP_NONE);
        _x += 10;
    };
};

void drawTextChar(char stuff, int x, int y) {
	// to-do: not this shit.
	// I should get to optimizing this
	SDL_SetTextureColorMod(sprites[0], 255, 255, 255);
    drawSpriteSheeted(sprites[0], x, y, stuff - 32, 10, 10, SDL_FLIP_NONE);
};

void drawNumber(int16_t x, int16_t y, int number) {

	char text[7];

	text[6] = 0; // terminate the string

	int8_t positive = 1;

	if (number < 0)
	{
		positive = 0;
		number *= -1;
	}

	int8_t position = 5;

	while (1)
	{
		text[position] = '0' + number % 10;
		number /= 10;

		position--;

		if (number == 0 || position == 0)
		  break;

		if (!positive)
		{
			text[position] = '-';
			position--;
		}
	}

	drawTextString(text, x, y);

	return 5 - position;
};

void draw() {
	drawSpriteSheeted(sprites[1], plr.x - cam_x, plr.y - cam_y, plr.animframe, 16, 16, (plr.pflags & PFLAG_FLIPPED));
	for (uint8_t i = 0; i < 63; i++) {
		if (!(interacts[i].flags & INTER_ACTIVE)) continue;
		switch (interacts[i].type)
		{
			case INTERTYPE_BRICK:
				drawSpriteSheeted(sprites[2], interacts[i].x - cam_x, interacts[i].y - cam_y, 0, 16, 16, 0);
				break;
			case INTERTYPE_ITEMBLOCK:
				drawSpriteSheeted(sprites[3], interacts[i].x - cam_x, interacts[i].y - cam_y, 0, 16, 16, 0);
				break;
		}
		drawSpriteSheeted(sprites[1], interacts[i].x - cam_x, interacts[i].y - cam_y, 0, 16, 16, 0);
		drawColBox(interacts[i].colbox, 1);
	};
	for (uint8_t i = 0; i < 255; i++) {
		if (!(lvlcol_flags[i] & INTER_ACTIVE)) continue;
		SDL_SetRenderDrawColor(render, 255, 255, 255, 255);
		drawColBox(lvlcollision[i], 1);
	};
	SDL_SetRenderDrawColor(render, 255, 0, 0, 255);
	drawColBox(plr.colbox, 1);
	SDL_SetRenderDrawColor(render, 0, 0x01, 0x90, 255);
	drawTextString("SCORE:", 0, 0);
	drawNumber(0, 16, score);
};

void keys() {
	if (keystates[SDL_SCANCODE_LEFT]) input_keys |= KEY_LF;
	if (keystates[SDL_SCANCODE_RIGHT]) input_keys |= KEY_RT;
	if (keystates[SDL_SCANCODE_UP]) input_keys |= KEY_UP;
	if (keystates[SDL_SCANCODE_DOWN]) input_keys |= KEY_DN;
	if (keystates[SDL_SCANCODE_Z]) input_keys |= KEY_JUMP;
	if (keystates[SDL_SCANCODE_X]) input_keys |= KEY_RUN;
	if (keystates[SDL_SCANCODE_ESCAPE]) input_keys |= KEY_MENU;
	
	/*if (controllerinput) {
	    if (SDL_JoystickGetButton(controllerinput, SDL_CONTROLLER_BUTTON_DPAD_LEFT) || SDL_JoystickGetAxis(controllerinput, SDL_CONTROLLER_AXIS_LEFTX) < -16383) input_keys |= KEY_LEFT;
	    if (SDL_JoystickGetButton(controllerinput, SDL_CONTROLLER_BUTTON_DPAD_RIGHT) || SDL_JoystickGetAxis(controllerinput, SDL_CONTROLLER_AXIS_LEFTX) > 16383) input_keys |= KEY_RIGHT;
	    if (SDL_JoystickGetButton(controllerinput, SDL_CONTROLLER_BUTTON_DPAD_UP) || SDL_JoystickGetAxis(controllerinput, SDL_CONTROLLER_AXIS_LEFTY) < -16383) input_keys |= KEY_UP;
	    if (SDL_JoystickGetButton(controllerinput, SDL_CONTROLLER_BUTTON_DPAD_DOWN) || SDL_JoystickGetAxis(controllerinput, SDL_CONTROLLER_AXIS_LEFTY) > 16383) input_keys |= KEY_DOWN;
	    if (SDL_JoystickGetButton(controllerinput, SDL_CONTROLLER_BUTTON_A)) input_keys |= KEY_JUMP;
	    if (SDL_JoystickGetButton(controllerinput, SDL_CONTROLLER_BUTTON_START)) input_keys |= KEY_MENU;
	}*/
};

int main() {
	//if (SDL_Init(SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_EVENTS | MIX_INIT_MID | SDL_INIT_JOYSTICK) != 0) return 1;
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) return 1;
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 1, 2048) != 0)
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_WARNING, "No audio!", "The game couldn't find a usable audio system. Check your audio devices!", NULL);
	
	win = SDL_CreateWindow(GAME_NAME, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, GAME_WIDTH << 1, GAME_HEIGHT << 1, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
	render = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	
	SDL_RenderSetLogicalSize(render, GAME_WIDTH, GAME_HEIGHT); // keep that cwispy 270P 16:9 wesowutiown UwU
	
	SDL_SetRenderDrawBlendMode(render, SDL_BLENDMODE_NONE);
	
	//winsurf = SDL_GetWindowSurface(win);
	
	//if ((SDL_NumJoysticks() > 0)) controllerinput = SDL_JoystickOpen(0);
	
	sprites[0] = IMG_LoadTexture(render, "spr/fontmap.png");
	sprites[1] = IMG_LoadTexture(render, "spr/wendigo/small.png");
	sprites[2] = IMG_LoadTexture(render, "spr/block.png");
	sprites[3] = IMG_LoadTexture(render, "spr/itemblock.png");
	
	sfx[0] = Mix_LoadWAV("sfx/jump.wav");
	sfx[1] = Mix_LoadWAV("sfx/hurt.wav");
	sfx[2] = Mix_LoadWAV("sfx/stomp.wav");
	sfx[3] = Mix_LoadWAV("sfx/brickbreak.wav");
	
	music[0] = Mix_LoadMUS("HIKING.mod");
	music[1] = Mix_LoadMUS("HIKING.mid");
	if (!music[0]) printf("music[0] failed to load! (error %s)\n", Mix_GetError());
	if (!music[1]) printf("music[1] failed to load! (error %s)\n", Mix_GetError());

	SDL_SetRenderDrawColor(render, 0, 0x01, 0x90, 255);
	keystates = SDL_GetKeyboardState(NULL); // this was called every time keys() is ran, but apparently it's only needed to be called ONCE :)
	
	uint8_t running = 1;
	SDL_Event event;
	start();
	/*if (Mix_PlayMusic(music[0], -1) != 0) {
		printf("Can't play music! Trying MIDI... (error %s)\n", Mix_GetError());
		if (Mix_PlayMusic(music[1], -1) != 0) printf("Can't play MIDI! (error %s)\n", Mix_GetError());
	};*/
	while (running) {
		while (SDL_PollEvent(&event)) {
			switch (event.type)
			{
				case SDL_QUIT: running = 0; break;
			}
		}
		keys();
		draw();
		step();
		SDL_RenderPresent(render);
		SDL_RenderClear(render);
		SDL_Delay(16.667f);
	}
	
	SDL_DestroyRenderer(render);
	SDL_DestroyWindow(win);
	SDL_Quit();
	
	return 0;
};
