# !/bin/sh
# Make script for Wake to Hell, includes frontends and packaging
# Licensed under CC0

COMPILER="gcc"

if [ ! $1 ]; then
	echo "Please give an argument for the target frontend. (sdl, saf, etc)"
	exit 1
fi

if [ $1 = "sdl" ]; then
	$COMPILER main_sdl.c -lSDL2 -lSDL2_mixer -lSDL2_image $2 -o Wendigo
elif [ $1 = "saf" ]; then
	$COMPILER main_saf.c -lSDL2 $2 -o WendigoSAF
elif [ $1 = "package" ]; then
	BUILDDIR="BUILD/"
	if [ "$1" ]; then
		GAMEDIR="$1"
	fi

	cp Wendigo ${BUILDDIR}
	cp *.mid ${BUILDDIR}
	cp *.mod ${BUILDDIR}
	cp -r sfx ${BUILDDIR}
	cp -r sprites ${BUILDDIR}
	echo "The game (assuming your binary is named Wendigo) and all required files for a *NIX build are now in the $BUILDDIR folder."
elif [ $1 = "edit" ]; then
	$COMPILER lvleditor.c -lncurses -o mwjedit
elif [ $1 = "love" ]; then # lol
	echo "This is not how you are supposed to play the Wendigo."
fi
