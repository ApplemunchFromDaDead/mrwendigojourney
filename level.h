/*
This file manages the level format and loading in
levels to play from file, code, or memory.

MWJ_SUCKLESS_MODE uses levels put into the code directly,
akin to Anarch. No script exists yet to convert level files
to level code.
*/

FILE *lvl;

/*
LEVEL FORMAT
The MWJ level format is HEAVILY dependant on trusting
the program to load it correctly. It doesn't use safe-
guard bytes to explicitly indicate when to stop reading
except for the EOF, 0xFF.

== HEADER : 8 bytes ==
4 bytes - "MWJL" magic number
1 byte - Level flags
1 byte - Game version (see MWJ_VERSION macro in game.h)
1 byte - Object count
1 byte - Solid count

== BODY ==
1 byte - Object type
1 byte - Object flags
2 bytes - Object X in 16-bit integer format
2 bytes - Object Y in 16-bit integer format

== SOLIDS ==
1 byte - Object flags
1 byte - Object type
1 byte - X
1 byte - X scale
2 bytes - Y respective variables

Positions are multiplied by 16, so as to assume they are tiles
*/

#define MWJ_LEVELFLAG_AUTOSCROLL 1

void loadLevel(const char *LVLNAME, uint8_t posmul) {
	lvl = fopen(LVLNAME, "r");
	if (!lvl) printf("Issue loading level!");
	uint8_t lvl_header[8];
	fread(&lvl_header, 1, 8,lvl);
	uint8_t readbuffer[6];
	for (uint8_t i = 0; i < lvl_header[6]; i++) {
		fread(&readbuffer, 1, 6, lvl);
		uint8_t thing = addInteractible(readbuffer[1]);
		interacts[thing].x = readbuffer[2] * posmul;
		interacts[thing].y = readbuffer[4] * posmul;
		interacts[thing].colbox.x1 = interacts[thing].x;
		interacts[thing].colbox.y1 = interacts[thing].y;
		interacts[thing].colbox.x2 = interacts[thing].x + 16;
		interacts[thing].colbox.y2 = interacts[thing].y + 16;
	};
	for (uint8_t i = 0; i < lvl_header[7]; i++) {
		fread(&readbuffer, 1, 6, lvl);
		if (readbuffer[0] & INTER_ACTIVE) {
			if (readbuffer[3] == 0) readbuffer[3] = posmul;
			if (readbuffer[5] == 0) readbuffer[5] = posmul;
			addCollisionBox(readbuffer[2] * posmul, readbuffer[4] * posmul, readbuffer[3], readbuffer[5]);
		};
	};
	fclose(lvl);
};
