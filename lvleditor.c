// CLI-based level editor for MWJ levels

#include <stdint.h>
#include <stdio.h>
#include <errno.h>

#include "game.h"
#undef KEY_UP
#include <ncurses.h>

FILE *file;

uint8_t lvl_header[8];
uint8_t lvl_body[6 * 128];
uint8_t lvl_solids[6 * 128];

uint8_t editmode = 0; // 0 = interactibles, >1 = solids
uint8_t selectindex = 0;
uint8_t helpmenu = 0;

void drawChar(char c, uint8_t x, uint8_t y) {
  mvaddch(y, x, c);
};

void playSFX(uint8_t index) {};

void saveLevel() {
	file = fopen("lvl.dat", "w");
	if (!file) {
		printf("Cannot open file: error code %i", errno);
		return;
	};
	puts("Opened file");
	for (uint8_t i = 0; i < 127; i++) {
		if ((lvl_body[i * 6] & INTER_ACTIVE)) {
			lvl_header[6]++;
			puts("Incremented interactibles");
		};
		if ((lvl_solids[i * 6] & INTER_ACTIVE)) {
			lvl_header[7]++;
			puts("Incremented solids");
		};
	};
	fwrite(&lvl_header, 1, 8, file);
	puts("Wrote header");
	fwrite(&lvl_body, 1, 6 * lvl_header[6], file);
	puts("Write body");
	fwrite(&lvl_solids, 1, 6 * lvl_header[7], file);
	puts("Write solids");
	if (!fclose(file)) printf("Can't close file: error code %i", errno); return;
};

void loadLevel_EDITOR(const char *LVLNAME, uint8_t posmul) {
	lvl = fopen(LVLNAME, "r");
	if (!lvl) printf("Issue loading level!");
	uint8_t lvl_header[8];
	fread(&lvl_header, 1, 8,lvl);
	uint8_t curline = 8;
	// some basic fread trickery
	printf("Loading in interactibles...\n");
	fread(&lvl_body, 6 * lvl_header[6], 1, lvl);
	curline += 6 * lvl_header[6];
	printf("Current line: %i\n", curline);
	printf("Loading in solids...\n");
	fread(&lvl_solids, 6 * lvl_header[7], 1, lvl);
	curline += 6 * lvl_header[7];
	printf("Current line: %i\n", curline);
	fclose(lvl);
};

void main(int argc, char *argv[]) {
  WINDOW *win;
  
  win = initscr();
  noecho();
  nodelay(win, TRUE);
  
  init_pair(INTERTYPE_BRICK, COLOR_WHITE, COLOR_YELLOW);
  init_pair(INTERTYPE_ITEMBLOCK, COLOR_WHITE, COLOR_GREEN);
  
  start_color();
  
  if (argc > 1) {
  	if (argv[1][0] == '-' && argv[1][1] == 'h' && argv[1][2] == 0) {
    		puts("MWJEDIT: a level editor for MWJ");
    		puts("usage: ./mwjedit [level]\n");
		puts("Press H in the editor for keys");
    		endwin();
	    	return;
  	};
  	loadLevel_EDITOR(argv[1], 1);
  } else {
	lvl_solids[0] ^= INTER_ACTIVE;
	lvl_solids[3] = 20;
	lvl_solids[4] = 21;
	lvl_solids[5] = 1;
	lvl_body[0] ^= INTER_ACTIVE;
  };

  uint8_t running = 1;
  while (running) {
  	char input = getch();
  	if (!helpmenu) {
		switch (input) {
			case 'z': selectindex--; break;
			case 'c': selectindex++; break;
			case '4': cam_x--; break;
			case '6': cam_x++; break;
			case '8': cam_y--; break;
			case '5': cam_y++; break;
			case 'Q': running = 0; break;
			case 'G': saveLevel(); break;
			case 'f': if (editmode == 1) editmode = 0; else editmode = 1; break;
			case 'h': clear(); helpmenu = 1; break;
	   	};
	   	if (editmode == 0) {
			switch (input) {
				case 'w': lvl_body[selectindex * 6 + 4]--; break;
				case 's': lvl_body[selectindex * 6 + 4]++; break;
				case 'a': lvl_body[selectindex * 6 + 2]--; break;
				case 'd': lvl_body[selectindex * 6 + 2]++; break;
				case 'r':
					lvl_body[selectindex * 6] ^= INTER_ACTIVE;
					break;
				case 'q':
					lvl_body[selectindex * 6 + 1]--;
					break;
				case 'e':
					lvl_body[selectindex * 6 + 1]++;
					break;
		   	};
	   	} else {
			switch (input) {
				case 'w': lvl_solids[selectindex * 6 + 4]--; break;
				case 's': lvl_solids[selectindex * 6 + 4]++; break;
				case 'a': lvl_solids[selectindex * 6 + 2]--; break;
				case 'd': lvl_solids[selectindex * 6 + 2]++; break;
				// shift is held?
				case 'W': lvl_solids[selectindex * 6 + 5]--; break;
				case 'S': lvl_solids[selectindex * 6 + 5]++; break;
				case 'A': lvl_solids[selectindex * 6 + 3]--; break;
				case 'D': lvl_solids[selectindex * 6 + 3]++; break;
				case 'r':
					lvl_solids[selectindex * 6] ^= INTER_ACTIVE;
					break;
				case 'q':
					lvl_solids[selectindex * 6 + 1]--;
					break;
				case 'e':
					lvl_solids[selectindex * 6 + 1]++;
					break;
		   	};
	   	};
		clear();

		for (uint8_t i = 0; i < 128; i++) {
		  if (!(lvl_solids[i * 6] & INTER_ACTIVE)) continue;
		  if (selectindex == i && editmode) attron(A_BOLD);
		  for (uint8_t h = 0; h < lvl_solids[i * 6 + 5]; h++) {
		    for (uint8_t w = 0; w < lvl_solids[i * 6 + 3]; w++) {
		  	  drawChar('#', lvl_solids[i * 6 + 2] - cam_x + w, lvl_solids[i * 6 + 4] + h);
		    };
		  };
		  if (selectindex == i && editmode) attroff(A_BOLD);
		};

		for (uint8_t i = 0; i < 128; i++) {
		  if (!(lvl_body[i * 6] & INTER_ACTIVE)) continue;
		  if (selectindex == i && !editmode) attron(A_BOLD);
		  if (lvl_body[i * 6 + 1] <= 1) attron(COLOR_PAIR(lvl_body[i * 6 + 1]));
		  drawChar('!' + lvl_body[i * 6 + 1], lvl_body[i * 6 + 2] - cam_x, lvl_body[i * 6 + 4]);
		  if (selectindex == i && !editmode) attroff(A_BOLD);
		  if (lvl_body[i * 6 + 1] <= 1) attroff(COLOR_PAIR(lvl_body[i * 6 + 1]));
		};
		
		drawChar('P', 4 - cam_x, 20);

		drawChar(selectindex + '0', 0, 23);
		drawChar(cam_x + '0', 6, 23);
		drawChar(cam_y + '0', 8, 23);
		drawChar(editmode + '0', 14, 23);
    } else {
    	mvprintw(0, 0, "== MWJEDIT HELP ==");
    	mvprintw(2, 0, "WASD - Move selected thing");
	mvprintw(3, 0, "Q, E - Change object type");
    	mvprintw(4, 0, "SHIFT+WASD - Use move keys to change solid scale");
    	mvprintw(6, 0, "Numpad 8546 - Move camera");
    	mvprintw(7, 0, "Z, C - Move back and forth between things");
    	mvprintw(8, 0, "SHIFT+G - Save to lvl.dat");
    	mvprintw(9, 0, "H - Opens and closes this help menu");
    	mvprintw(10, 0, "SHIFT+Q - Quit");
    	if (getch() == 'h') helpmenu = 0;
    };
    refresh();
    usleep(1 / 30);
  };
  endwin();
};
